import Vue from 'vue'
import Vuex from 'vuex'
import { sampleSize } from 'lodash'

import { getPhotos } from './api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    photos: []
  },
  mutations: {
    setPhotos: (state, photos) => {
      state.photos = photos
    }
  },
  actions: {
    loadPhotos: async ({ commit }) => {
      const photos = await getPhotos()
      commit('setPhotos', photos)
    }
  },
  getters: {
    getRandomPhotos: state => amount => sampleSize(state.photos, amount)
  }
})
