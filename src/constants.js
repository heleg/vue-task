export const Breakpoints = {
  SM: 576,
  MD: 768,
  LG: 992,
  XL: 1200
}

export const GALLERY_IMG_RATIO = 9 / 16
