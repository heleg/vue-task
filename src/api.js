import axios from 'axios'
import { range } from 'lodash'
import queryString from 'query-string'

import { Breakpoints, GALLERY_IMG_RATIO } from './constants'

const request = axios.create({
  baseURL: 'https://api.unsplash.com/',
  headers: {
    Authorization:
      'Client-ID 9c05467cb3d94badbd5e12dc9826e401e78bb13d840dbb108db5ee59556d5824'
  }
})

request.interceptors.response.use(response => response.data)

export async function getPhotos() {
  const photos = await request.get('/collections/3137488/photos', {
    params: {
      per_page: 15
    }
  })
  return photos.map(({ urls: { full } }) => queryString.parseUrl(full).url)
}

export function createImageUrls(baseUrl) {
  return {
    [Breakpoints.MD]: createUrlsForSize({ baseUrl, size: Breakpoints.MD }),
    [Breakpoints.LG]: createUrlsForSize({ baseUrl, size: Breakpoints.LG })
  }
}

function createUrlsForSize({ baseUrl, size }) {
  return range(3).reduce((result, i) => {
    const width = size * (i + 1)
    const height = width * GALLERY_IMG_RATIO

    const url = createImageUrl({ baseUrl, w: width, h: height })
    result[`x${i + 1}`] = { width, height, url }

    return result
  }, {})
}

function createImageUrl({ baseUrl, w, h }) {
  const query = queryString.stringify({
    w,
    h,
    crop: 'faces',
    fit: 'crop',
    q: 80
  })
  return `${baseUrl}?${query}`
}
